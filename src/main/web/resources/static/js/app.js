var app = angular.module('flightApp', ['ngResource']);

app.factory('User', ['$resource', function ($resource) {
    return $resource('http://localhost:8080/user/:userId', {userId: '@uid'},
        {
            updateUser: {method: 'PUT'}
        }
    );
}]);

app.factory('Flight', ['$resource', function ($resource) {
    return $resource('http://localhost:8080/flight/:flightId', {flightId: '@uid'},
        {
            updateFlight: {method: 'PUT'}
        }
    );
}]);

app.factory('Login', ['$resource', function ($resource) {
    return $resource('http://localhost:8080/', {username: 'admin', password: 'admin'},
        {
            login: {method: 'POST'}
        }
    );
}]);

app.controller('UserController', ['$scope', 'User', '$http', function ($scope, User, $http) {
    var ob = this;
    ob.users = [];
    ob.user = new User();
    ob.fetchAllUsers = function () {
        ob.users = User.query();
    };
    ob.fetchAllUsers();
    ob.addUser = function () {
        console.log('Inside save');
        if ($scope.userForm.$valid) {
            $http({
                url: "/user",
                method: "POST",
                data: {
                    user: ob.user,
                    action: "add"
                }
        }).success(function (user) {
                console.log(user);
                ob.flag = 'created';
                ob.reset();
                ob.fetchAllUsers();
            }).error(function (err) {
                console.log(err.status);
                ob.flag = 'failed';
            });
        }
    };
    ob.editUser = function (id) {
        console.log('Inside edit');
        ob.user = User.get({userId: id}, function () {
            ob.flag = 'edit';
        });
    };
    ob.updateUserDetail = function () {
        console.log('Inside update');
        if ($scope.userForm.$valid) {
            ob.user.$updateUser(function (user) {
                console.log(user);
                ob.updatedId = user.pid;
                ob.reset();
                ob.flag = 'updated';
                ob.fetchAllUsers();
            });
        }
    };
    ob.deleteUser = function (id) {
        console.log('Inside delete');
        ob.user = User.delete({userId: id}, function () {
            ob.reset();
            ob.flag = 'deleted';
            ob.fetchAllUsers();
        });
    };
    ob.reset = function () {
        ob.user = new User();
        $scope.userForm.$setPristine();
    };
    ob.cancelUpdate = function (id) {
        ob.user = new User();
        ob.flag = '';
        ob.fetchAllUsers();
    };
}]);

app.controller('FlightController', ['$scope', 'Flight', '$http', function ($scope, Flight, $http) {
    var ob = this;
    ob.flights = [];
    ob.flight = new Flight();
    ob.fetchAllFlights = function () {
        ob.flights = Flight.query();
    };
    ob.fetchAllFlights();

    $http.get({
        url: "/flight",
        method: "GET",
        params: {name: "getCities"}
    });

    ob.addFlight = function () {
        console.log('Inside save');
        if ($scope.flightForm.$valid) {
            $http.post("/flight", ob.flight).success(function (flight) {
                console.log(flight);
                ob.flag = 'created';
                ob.reset();
                ob.fetchAllFlights();
            }).error(function (err) {
                console.log(err.status);
                ob.flag = 'failed';
            });
        }
    };
    ob.editFlight = function (id) {
        console.log('Inside edit');
        ob.flight = Flight.get({flightId: id}, function () {
            ob.flag = 'edit';
        });
    };
    ob.updateFlightDetail = function () {
        console.log('Inside update');
        if ($scope.flightForm.$valid) {
            ob.flight.$updateFlight(function (flight) {
                console.log(flight);
                ob.updatedId = flight.pid;
                ob.reset();
                ob.flag = 'updated';
                ob.fetchAllFlights();
            });
        }
    };
    ob.deleteFlight = function (id) {
        console.log('Inside delete');
        ob.flight = Flight.delete({flightId: id}, function () {
            ob.reset();
            ob.flag = 'deleted';
            ob.fetchAllFlights();
        });
    };
    ob.reset = function () {
        ob.flight = new Flight();
        $scope.flightForm.$setPristine();
    };
    ob.cancelUpdate = function (id) {
        ob.flight = new Flight();
        ob.flag = '';
        ob.fetchAllFlights();
    };
}]);

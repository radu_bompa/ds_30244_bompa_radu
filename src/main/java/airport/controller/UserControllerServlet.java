package airport.controller;

import airport.dao.UserDAO;
import airport.entity.Role;
import airport.entity.User;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.stream.JsonParser;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class UserControllerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		JsonReader jsonReader = Json.createReaderFactory(new HashMap<String, String>()).createReader(request.getReader());
		JsonObject requestParams = jsonReader.readObject();

		String username = requestParams.getString("username");
		String password = requestParams.getString("password");
		String firstName = requestParams.getString("firstName");
		String lastName = requestParams.getString("lastName");
		String role = requestParams.getString("role");


		if (username != null && password != null && firstName != null && lastName != null && role != null) {
			HttpSession session = request.getSession(true);
			try {
				UserDAO userDAO = new UserDAO();
				userDAO.addUser(username, password, lastName, firstName, Role.valueOf(role));
				response.sendRedirect("Success");
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

	}

	protected void doGet(HttpServletRequest request,
	                     HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");

		RequestDispatcher view;
		if (username != null) {
			UserDAO userDAO = new UserDAO();
			User user = userDAO.getUser(username);
			if (user != null) {
				Role userRole = user.getRole();
				if (Role.ADMIN.equals(userRole)) {
					view = request.getRequestDispatcher("WEB-INF/static/html/userManagement.html");
					List<User> users = userDAO.getUsers();
					request.setAttribute("users", users);
				} else {
					view = request.getRequestDispatcher("WEB-INF/static/html/503.html");
				}
			} else {
				view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
			}
		} else {
			view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
		}
		view.forward(request, response);
	}
}

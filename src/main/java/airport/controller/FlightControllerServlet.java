package airport.controller;

import airport.dao.FlightDAO;
import airport.dao.UserDAO;
import airport.entity.City;
import airport.entity.Role;
import airport.entity.User;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import java.util.List;

public class FlightControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
	                     HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		RequestDispatcher view;
		if (username != null) {
			UserDAO userDAO = new UserDAO();
			User user = userDAO.getUser(username);
			if (user != null) {
				Role userRole = user.getRole();
				if (Role.ADMIN.equals(userRole)) {
					if (request.getParameter("name").equals("getCities")) {
						FlightDAO flightDAO = new FlightDAO();
						List<City> cities = flightDAO.getCities();
						String citiesJson = "";
						for (City city : cities) {
							citiesJson += "{ " + "\"id:\"" + city.getId() + ";" +
									"\"name\":" + city.getName() + "}";
						}

						request.setAttribute("cities", citiesJson);
						response.setContentType("application/json");
						response.getWriter().write(citiesJson);
					} else {
						view = request.getRequestDispatcher("WEB-INF/static/html/flightManagement.html");
						view.forward(request, response);
					}
				} else {
					view = request.getRequestDispatcher("WEB-INF/static/html/503.html");
					view.forward(request, response);
				}
			} else {
				view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
				view.forward(request, response);
			}
		} else {
			view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
			view.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		JsonReader jsonReader = Json.createReaderFactory(new HashMap<String, String>()).createReader(request.getReader());
		JsonObject requestParams = jsonReader.readObject();

		String flightNumber = requestParams.getString("flightNumber");
		String airplaneType = requestParams.getString("airplaneType");
		String departureCity = requestParams.getString("departureCity");
		Date departureTime = Date.valueOf(requestParams.getString("departureTime"));
		String arrivalCity = requestParams.getString("arrivalCity");
		Date arrivalTime = Date.valueOf(requestParams.getString("arrivalTime"));

		if (flightNumber != null && airplaneType != null && departureCity != null && departureTime != null
				&& arrivalCity != null && arrivalTime != null) {
			HttpSession session = request.getSession(true);
			try {
				FlightDAO flightDAO = new FlightDAO();
				flightDAO.addFlight(flightNumber, airplaneType, departureCity, departureTime, arrivalCity, arrivalTime);
				response.sendRedirect("Success");
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

	}
}

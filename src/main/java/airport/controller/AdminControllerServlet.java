package airport.controller;

import airport.dao.UserDAO;
import airport.entity.Role;
import airport.entity.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminControllerServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        RequestDispatcher view;
        if (username != null) {
            UserDAO userDAO = new UserDAO();
            User user = userDAO.getUser(username);
            if (user != null) {
                Role userRole = user.getRole();
                if (Role.ADMIN.equals(userRole)) {
                    view = request.getRequestDispatcher("WEB-INF/static/html/admin.html");
                } else {
                    view = request.getRequestDispatcher("WEB-INF/static/html/503.html");
                }
            } else {
                view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
            }
        } else {
            view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
        }
        view.forward(request, response);
    }
}

package airport.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class ClientControllerServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request,
	                     HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String username = (String) session.getAttribute("username");
		RequestDispatcher view;
		if (username != null) {
			view = request.getRequestDispatcher("WEB-INF/static/html/client.html");
		} else {
			view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
		}
		view.forward(request, response);
	}
}

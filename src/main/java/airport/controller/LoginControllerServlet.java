package airport.controller;

import airport.dao.UserDAO;
import airport.entity.Role;
import airport.entity.User;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

public class LoginControllerServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserDAO userDAO = new UserDAO();
        User user = userDAO.getUser(username);
        RequestDispatcher view = request.getRequestDispatcher("503");
        if (password.equals(user.getPassword())) {
            Role userRole = user.getRole();
            HttpSession session = request.getSession();
            session.setAttribute("username", username);
            if (Role.ADMIN.equals(userRole)) {
                view = request.getRequestDispatcher("WEB-INF/static/html/admin.html");
            } else if (Role.CLIENT.equals(userRole)) {
                response.sendRedirect("/client");
                view = request.getRequestDispatcher("WEB-INF/static/html/client.html");
            }
        } else {
            view = request.getRequestDispatcher("WEB-INF/static/html/login.html");
        }

        view.forward(request, response);
    }
}

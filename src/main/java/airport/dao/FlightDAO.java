package airport.dao;

import airport.entity.City;
import airport.entity.Role;
import airport.entity.Flight;
import airport.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.transaction.Transactional;
import java.sql.Date;
import java.util.Arrays;
import java.util.List;

@Transactional
public class FlightDAO {

	public void addFlight(String flightNumber, String airplaneType, String departureCity, Date departureTime, String arrivalCity,
	                      Date arrivalTime) {
		try {
			Session session = HibernateUtil.createSession();
			Transaction transaction = session.beginTransaction();
			Flight flight = new Flight();
			flight.setFlightNumber(flightNumber);
			flight.setAirplaneType(airplaneType);
			flight.setDepartureCity(departureCity);
			flight.setDepartureTime(departureTime);
			flight.setArrivalCity(arrivalCity);
			flight.setArrivalTime(arrivalTime);
			session.save(flight);
			transaction.commit();
			System.out.println("\n\n Flight added \n");

		} catch (HibernateException e) {
			System.out.println(Arrays.toString(e.getStackTrace()));
			System.out.println("error");
		}

	}

	public List<City> getCities() {
		try {
			Session session = HibernateUtil.createSession();
			return session.createCriteria(City.class).list();
		} catch (HibernateException e) {
			System.out.println(Arrays.toString(e.getStackTrace()));
			System.out.println("error");
		} finally {

		}
		return null;
	}
}

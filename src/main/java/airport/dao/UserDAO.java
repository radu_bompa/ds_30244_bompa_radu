package airport.dao;

import airport.entity.Role;
import airport.entity.User;
import airport.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;

@Transactional
public class UserDAO {

	public void addUser(String username, String password, String firstName, String lastName, Role role) {
		try {
			Session session = HibernateUtil.createSession();
			Transaction transaction = session.beginTransaction();
			User user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setRole(role);
			session.save(user);
			transaction.commit();
			System.out.println("\n\n User added \n");

		} catch (HibernateException e) {
			System.out.println(Arrays.toString(e.getStackTrace()));
			System.out.println("error");
		}

	}

	public User getUser(String username) {
		Session session = HibernateUtil.createSession();
		Transaction transaction = session.beginTransaction();
		Query query = session.createQuery("from User where username = :username ");
		query.setParameter("username", username);
		List userList = query.list();
		transaction.commit();
		if (!userList.isEmpty()) {
			return (User) userList.get(0);
		} else {
			return null;
		}
	}

	public List<User> getUsers() {
		Session session = HibernateUtil.createSession();
		Transaction transaction = session.beginTransaction();
		List<User> users = session.createCriteria(User.class).list();
		transaction.commit();
		return users;
	}
}
